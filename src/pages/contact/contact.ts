import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  public send(): void {
    this.toastCtrl.create({
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      duration: 5000,
      showCloseButton: true,
      message: 'Sua mensagem foi enviada para nossa equipe, agradecemos pelo seu contato.'
    }).present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

}
