import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DownloadPatientsPage } from './download-patients';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DownloadPatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(DownloadPatientsPage),
    ComponentsModule
  ],
})
export class DownloadPatientsPageModule {}
