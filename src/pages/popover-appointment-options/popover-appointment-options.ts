import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';


/**
 * Generated class for the PopoverAppointmentOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover-appointment-options',
  templateUrl: 'popover-appointment-options.html',
})
export class PopoverAppointmentOptionsPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController
  ) {
  }

  public dismiss(): void {
    this.viewCtrl.dismiss();
  }

  public cancelAppointment(): void {
    const alert = this.alertCtrl.create({
      buttons: [
        'Voltar',
        'Remover'
      ],
      title: 'Tem certeza que deseja remover este compromisso?',
      message: 'Você está prestes a remover o compromisso com o paciente Antonio Valentim. Tem certeza que deseja continuar esta ação?'
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverProcedureOptionsPage');
  }

}
