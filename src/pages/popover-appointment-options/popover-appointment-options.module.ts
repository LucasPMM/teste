import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverAppointmentOptionsPage } from './popover-appointment-options';

@NgModule({
  declarations: [
    PopoverAppointmentOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverAppointmentOptionsPage),
  ],
})
export class PopoverAppointmentOptionsPageModule {}
