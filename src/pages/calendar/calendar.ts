import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CalendarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
})
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

  public state: string = 'listdates';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public setState(state): void {
    this.state = state;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
    const prev = this.navCtrl.getAllChildNavs();
    console.log(prev);
  }

}
