import { ListDatesPage } from '../list-dates/list-dates';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarPage } from './calendar';

@NgModule({
  declarations: [
    CalendarPage,
    ListDatesPage
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CalendarPage)
  ],
})
export class CalendarPageModule {}
