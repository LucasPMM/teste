import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientProtocolsPage } from './patient-protocols';

@NgModule({
  declarations: [
    PatientProtocolsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientProtocolsPage),
  ],
})
export class PatientProtocolsPageModule {}
