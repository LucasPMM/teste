import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public toastCtrl: ToastController) {
  }

  public login(): void {
    this.navCtrl.setRoot('HomePage');
  }

  private successForgotPassword(): void {
    this.toastCtrl.create({
      dismissOnPageChange: true,
      duration: 3000,
      message: 'Uma senha temporária foi enviada para o seu email, caso demore mais queo comum verifique sua caixa de spam.',
      closeButtonText: 'Ok',
      showCloseButton: true
    }).present();
  }

  public forgotPassword(): void {
    let prompt = this.alertCtrl.create({
      title: 'Esqueci minha senha',
      message: "Informe o seu email. Uma nova senha temporária será enviada ao seu e-mail para que você possa acessar sua conta.",
      inputs: [{
          name: 'Email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [{
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Enviar',
        handler: data => {
          this.successForgotPassword();
          console.log('Saved clicked');
        }
      }]
    });
    prompt.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
