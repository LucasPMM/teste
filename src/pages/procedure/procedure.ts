import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, PopoverController } from 'ionic-angular';
import { GalleryModal } from 'ionic-gallery-modal';

/**
 * Generated class for the ProcedurePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-procedure',
  templateUrl: 'procedure.html',
})
export class ProcedurePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController
  ) {}

  public options(e): void {
    const pop = this.popoverCtrl.create('PopoverProcedureOptionsPage');
    pop.present({
      ev: e
    });
  }

  public openGallery(index): void {
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: [
        {
          url: 'assets/joelho1.jpg',
          title: 'Joelho 1'
        },
        {
          url: 'assets/joelho2.jpg',
          title: 'Joelho 2'
        }
      ],
      initialSlide: index || 0
    });
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProcedurePage');
  }

}
