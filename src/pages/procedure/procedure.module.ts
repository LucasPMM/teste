import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { ProcedurePage } from './procedure';




@NgModule({
  declarations: [
    ProcedurePage,
  ],
  imports: [
    IonicPageModule.forChild(ProcedurePage),
    ionicGalleryModal.GalleryModalModule
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig,
    }
  ]
})
export class ProcedurePageModule {}
