import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProcedurePage } from './create-procedure';

@NgModule({
  declarations: [
    CreateProcedurePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateProcedurePage),
  ],
})
export class CreateProcedurePageModule {}
