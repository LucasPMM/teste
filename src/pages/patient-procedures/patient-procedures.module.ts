import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientProceduresPage } from './patient-procedures';

@NgModule({
  declarations: [
    PatientProceduresPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientProceduresPage),
  ],
})
export class PatientProceduresPageModule {}
