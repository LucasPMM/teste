import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverProcedureOptionsPage } from './popover-procedure-options';

@NgModule({
  declarations: [
    PopoverProcedureOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverProcedureOptionsPage),
  ],
})
export class PopoverProcedureOptionsPageModule {}
