import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PatientPage } from './patient';
import { PatientDetailsPage } from '../patient-details/patient-details';
import { PatientProceduresPage } from '../patient-procedures/patient-procedures';
import { PatientScoresPage } from '../patient-scores/patient-scores';
import { PatientProtocolsPage } from '../patient-protocols/patient-protocols';

@NgModule({
  declarations: [
    PatientPage,
    PatientDetailsPage,
    PatientProceduresPage,
    PatientScoresPage,
    PatientProtocolsPage
  ],
  imports: [
    IonicPageModule.forChild(PatientPage),
  ],
})
export class PatientPageModule {}
