import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsPage } from './patients';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientsPage),
    ComponentsModule
  ],
})
export class PatientsPageModule {}
