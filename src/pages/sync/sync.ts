import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SyncPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sync',
  templateUrl: 'sync.html',
})
export class SyncPage {

  public synced: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public sync(): void {
    this.synced = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SyncPage');
  }

}
