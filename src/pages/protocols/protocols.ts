import { Component } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the ProtocolsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-protocols',
  templateUrl: 'protocols.html',
})
export class ProtocolsPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController
  ) {}

  private showSharedProtocol(): void {
    this.alertCtrl.create({
      buttons: ['Recusar', 'Aceitar'],
      title: 'Compartilhamentos',
      subTitle:  '1/1',
      message: 'O Guilherme Valentim deseja compartilhar com você o protocolo "Calcâneo". Você deseja aceitar?'
    }).present();
  }

  private checkSharedProtocols(): void {
    const toast = this.toastCtrl.create({
      closeButtonText: 'Ver',
      dismissOnPageChange: true,
      message: 'Você tem uma solicitação de compartilhamento aguardando sua resposta.',
      showCloseButton: true
    });
    toast.present();
    toast.onDidDismiss((data, role) => {
      switch (role) {
        case 'close':
          this.showSharedProtocol();
          break;
      }
    });
  }

  public openDownloadPage(): void {
    this.modalCtrl.create('DownloadProtocolsPage').present();
  }

  ionViewDidEnter(){
    this.checkSharedProtocols();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProtocolsPage');
  }

}
