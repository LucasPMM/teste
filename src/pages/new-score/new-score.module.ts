import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewScorePage } from './new-score';

@NgModule({
  declarations: [
    NewScorePage,
  ],
  imports: [
    IonicPageModule.forChild(NewScorePage),
  ],
})
export class NewScorePageModule {}
