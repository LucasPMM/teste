import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientScoresPage } from './patient-scores';

@NgModule({
  declarations: [
    PatientScoresPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientScoresPage),
  ],
})
export class PatientScoresPageModule {}
