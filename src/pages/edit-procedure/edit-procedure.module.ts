import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProcedurePage } from './edit-procedure';

@NgModule({
  declarations: [
    EditProcedurePage,
  ],
  imports: [
    IonicPageModule.forChild(EditProcedurePage),
  ],
})
export class EditProcedurePageModule {}
