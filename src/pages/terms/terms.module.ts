import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsPage } from './terms';
import { IonPluriutilsModule } from '@pluritech/ion-pluriutils';

@NgModule({
  declarations: [
    TermsPage,
  ],
  imports: [
    IonicPageModule.forChild(TermsPage),
    IonPluriutilsModule
  ],
})
export class TermsPageModule {}
