import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProtocolPage } from './create-protocol';

@NgModule({
  declarations: [
    CreateProtocolPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateProtocolPage),
  ],
})
export class CreateProtocolPageModule {}
