import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DownloadProtocolsPage } from './download-protocols';

@NgModule({
  declarations: [
    DownloadProtocolsPage,
  ],
  imports: [
    IonicPageModule.forChild(DownloadProtocolsPage),
  ],
})
export class DownloadProtocolsPageModule {}
