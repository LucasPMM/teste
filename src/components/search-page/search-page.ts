import { Component } from '@angular/core';

/**
 * Generated class for the SearchPageComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'search-page',
  templateUrl: 'search-page.html'
})
export class SearchPageComponent {

  public terms: any[] = [];

  constructor() {
    console.log('Hello SearchPageComponent Component');
  }

  public search(ev: Event): void {
    let input = ev.target as HTMLInputElement;
    if (input.value) {
      this.terms = [input.value];
    } else {
      this.terms = [];
    }
  }
}
