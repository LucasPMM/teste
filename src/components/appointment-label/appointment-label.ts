import { Component, Input } from '@angular/core';

/**
 * Generated class for the AppointmentLabelComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'appointment-label',
  templateUrl: 'appointment-label.html'
})
export class AppointmentLabelComponent {

  @Input() name: string;
  @Input() color: string;
  @Input() desc: string;

  constructor() {
  }

}
