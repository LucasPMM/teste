import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { SearchPageComponent } from './search-page/search-page';
import { AppointmentLabelComponent } from './appointment-label/appointment-label';
@NgModule({
	declarations: [SearchPageComponent,
    AppointmentLabelComponent],
	imports: [IonicModule],
	exports: [SearchPageComponent,
    AppointmentLabelComponent]
})
export class ComponentsModule {}
